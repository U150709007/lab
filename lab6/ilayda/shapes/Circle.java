package ilayda.shapes;

public class Circle {
    public int radius;
	
	public Circle (int radius){
		this.radius=radius;
	}
	
	public double area(){
		return Math.PI * radius * radius;
	}
	public int getRadius(){
		return radius;
	}

}
