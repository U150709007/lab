package ilayda.shapes;

public class Rectangle {

	protected int side1;
	protected int side2;
	
	public Rectangle (int sideA,int sideB){
		this.side1=sideA;
		this.side2=sideB;
	}
	
	public double area(){
		return side1 * side2;
    
	}
}
