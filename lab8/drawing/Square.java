package drawing;

public class Square extends Shape {

	private int length;
	
	Point topLeft;
	
	public Square(int length, Point topLeft){
		this.length = length;
		this.topLeft = topLeft;
	}

	public void draw(){
		System.out.println("Drawing Square at " + topLeft + " having side length: " + length);
	}
	
	public double area(){
		return length * length;
	}
	
	public void move(int xDistance,  int yDistance){
		topLeft.move(xDistance, yDistance);
	}	

}