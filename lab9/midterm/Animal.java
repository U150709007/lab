package midterm;

public abstract class Animal {
  public abstract String getName();
  public abstract String speak();
}
