package ilayda.shapes3d;

import ilayda.shapes.Rectangle;

public class Box extends Rectangle {
	int side3;

	public Box(int side1, int side2,int side3) {
		super(side1, side2);
		this.side3 = side3;
	}
	
	public double area(){
		return 2 * (side1*side2 + side1*side3 + side2*side3);
	}
	
	
	
	public double volume(){
		return super.area() * side3;
	}

}
